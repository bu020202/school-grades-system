
Added method stubs for some unimplemented features in SQLHandler.java,

addStudent(...),

addSubject(...),

enrollStudentOnSubject(...),

addUpdateAttendance(...),

addUpdateGrade(...),


If you're unsure of what to do next try implementing these methods.


Regards,

Daniel