package SchoolSystem;

import SchoolSystemGUI.*;

/**
 * 
 * @author Daniel Brittain
 */
public class SystemController {
    
    private static SQLHandler sqlHandler;
    
    public static void main(String[] args){
        
        sqlHandler = new SQLHandler();
        //sqlHandler.openConnection();
        //int[] subjects = new int[3];
        //subjects[0] = 1;
        //subjects[1] = 2;
        //subjects[2] = 3;
        //sqlHandler.addStudent(1501006, "JimTest", subjects);
        new GUI_Controller();

    }
    
    public static SQLHandler getSQLHandler() {
        return sqlHandler;
    }
}