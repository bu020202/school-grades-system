//package SchoolSystem;
//
//import java.awt.GridBagConstraints;
//import java.awt.GridBagLayout;
//import java.awt.Insets;
//import javax.swing.BorderFactory;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//
///**
// *
// * @author Daniel Brittain
// */
//public class StudentView {
//
//    private int studentID;
//    private String studentName;
//    private double gradeAverage;
//    private double attendanceAverage;
//    private String grades;
//    
//    public StudentView() {
//        
//    }
//    
//    public StudentView(Login login, String grades) {
//        studentID = login.getUseriD();
//        this.grades = grades;
//        createAndShowStudentView();
//    }
//    
//    public StudentView(int StudentID, String studentName, double gradeAverage, 
//                                    double attendanceAverage, String grades)  {
//        
//        this.studentID = StudentID;
//        this.studentName = studentName;
//        this.gradeAverage = gradeAverage;
//        this.attendanceAverage = attendanceAverage;
//        this.grades = grades;
//        createAndShowStudentView();
//    }
//    
//    private void createAndShowStudentView() {
//        
//        JFrame viewFrame = new JFrame(studentName + " - Grades");
//        viewFrame.setLayout(new GridBagLayout());
//        GridBagConstraints gbc = new GridBagConstraints();
//        
//        JLabel summary = new JLabel();
//        summary.setBorder(BorderFactory.createTitledBorder("Summary"));
//        summary.setText(createSummary());
//        gbc.gridx=0;
//        gbc.gridy=0;
//        gbc.insets=new Insets(20,20,20,20);
//        viewFrame.add(summary, gbc);
//        
//        JLabel g = new JLabel(grades);
//        gbc.gridx=0;
//        gbc.gridy=1;
//        gbc.insets=new Insets(20,20,20,20);
//        viewFrame.add(g, gbc);
//        
//        viewFrame.pack();
//        viewFrame.setVisible(true);
//        
//        
//        
//    }
//    
//    private String createSummary() {
//        return "<html>" +
//                 "<body>" +
//                   "<table>" +
//                    "<tr>" +
//                      "<td>Student: " + studentName + "</td>" +
//                      "<td>Grade Point Average (GPA): " + gradeAverage + "</td>" +
//                    "</tr>" +
//                    "<tr>" +
//                      "<td>ID: " + studentID + "</td>" +
//                      "<td>Average Attendance: " + attendanceAverage + "%</td>" +
//                    "</tr>" +
//                    "</table>" +
//                  "</body>" +
//                "</html>";
//    }
//}
