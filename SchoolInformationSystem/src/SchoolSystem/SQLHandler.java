package SchoolSystem;

import SchoolSystemData.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * @author Daniel Brittain
 * @author Jake Leaning
 */
public class SQLHandler {
    
    private final String ORACLE_DB_URL = "jdbc:oracle:thin:@crusstuora1.staffs.ac.uk:1521:stora",
                         ORACLE_DB_UNAME = "L017063E",
                         ORACLE_DB_PWORD = "L017063E",
            
                         JAVA_DB_URL = "jdbc:derby://localhost:1527/SchoolSystem",
                         JAVA_DB_UNAME = "uname",
                         JAVA_DB_PWORD = "pword";
    
    private Connection con;
    
    public SQLHandler() {

    }
    
    public int loginUser(String username, char[]password) {
        
        int userId = -1;

        String sqlStr = "SELECT * FROM login " + 
                        "WHERE username='" + username.toUpperCase() + "'";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {          
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();

            while (rs.next()) {

                String storedPassword = rs.getString("Password");

                if(String.valueOf(password).equals(storedPassword)) {
                    userId = rs.getInt("ID");                           
                }            
            }

            rs.close();
            stmt.close();
        }
        catch (SQLException sqle) {

        }

        return userId;
    }
    
    public ArrayList<Student> getAllStudents() {
        
        ArrayList<Student> students = new ArrayList<>();

        String sqlStr = "SELECT studentID " +
                        "FROM student";

        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();

            while (rs.next()) {

                int studentId = rs.getInt("StudentId");
                Student student = populateStudentData(studentId);             
                students.add(student);            
            }

            rs.close();
            stmt.close();
        }
        catch (SQLException sqle) {

        }

        return students;
    }
    
    public Student populateStudentData(int studentID) {
        
        Student student = new Student(studentID);
        String studentName = getStudentName(studentID);      
        student.setName(studentName);       
        
        String sqlStr = "SELECT * " +
                        "FROM student_grades_attendance " +
                        "INNER JOIN subject " +
                        "ON student_grades_attendance.subjectid = subject.subjectid " +
                        "AND studentid = " + studentID +
                        "ORDER BY schoolyear DESC ";
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();
   
            int currentYear = -1;
            AcademicYear academicYear = null;
            
            while(rs.next()) {
                
                int schoolYear = rs.getInt("SchoolYear");
                
                if(schoolYear != currentYear) {
                    academicYear = student.addAcademicYear(schoolYear);
                    currentYear = schoolYear;
                }
                
                int subjectId = rs.getInt("SubjectID");
                String subjectName = rs.getString("SubjectName");
                double[] grades = retriveSubjectGrades(rs);
                double[] attendance = retriveSubjectAttendance(rs);
                
                academicYear.addSubject(subjectId,subjectName, grades, attendance);
            }
            
            rs.close();
            stmt.close();
        }
        catch(SQLException sqle) {
            System.out.println("Error");
        }
        
        calculateGpaForEachAcademicyear(student);
        calculateAttendanceForEachAcademicyear(student);
        
        return student;
    }
    
    private double[] retriveSubjectGrades (ResultSet rs) throws SQLException {
        
        double[] grades = new double[6];
        
        for(int i=4;i<10;i++) {
            
            String grade = rs.getString(i);           
            grades[i-4] = (grade != null) ? Double.parseDouble(grade) : -1;          
        }
        
        return grades;
    }
    
    private double[] retriveSubjectAttendance (ResultSet rs) throws SQLException {
        
        double[] attendance = new double[6];
        
        for(int i=10;i<16;i++) {
            
            String att = rs.getString(i);           
            attendance[i-10] = (att != null) ? Double.parseDouble(att) : -1;           
        }
        
        return attendance;
    }
    
    private void calculateGpaForEachAcademicyear(Student student) {
        
        ArrayList<AcademicYear> schoolYears = student.getAcademicYears();
        
        for(AcademicYear year : schoolYears) {
            
            ArrayList<Subject> subjects = year.getSubjects();           
            int count = 0;
            double total = 0.0;
            
            for(Subject subject : subjects) {
                
                double[] grades = subject.getGrades();
                
                for(int i=0;i<6;i++) {
                    
                    if(grades[i] >=0) {
                        
                        count++;
                        total += grades[i];
                    }
                }
            }
            
            
            DecimalFormat df = new DecimalFormat("#.0"); 
            
            double gpa;
            
            try {
                gpa = Double.parseDouble(df.format(total / count));

            }
            catch(NumberFormatException nfe) {
                gpa = 0;
            }
            

            year.setStudentGradePointAverage(gpa);
        }
        
        double currentGpa = schoolYears.get(0).getStudentGradePointAverage();
        
        student.setCurrentGradePointAverage(currentGpa);
    }
    
    private void calculateAttendanceForEachAcademicyear(Student student) {
        
        ArrayList<AcademicYear> schoolYears = student.getAcademicYears();
        
        for(AcademicYear year : schoolYears) {
            
            ArrayList<Subject> subjects = year.getSubjects();           
            int count = 0;
            double total = 0.0;
            
            for(Subject subject : subjects) {
                
                double[] attendance = subject.getAttendanceRecord();
                
                for(int i=0;i<6;i++) {
                    
                    if(attendance[i] >=0) {
                        
                        count++;
                        total += attendance[i];
                    }
                }
            }
            
            
            DecimalFormat df = new DecimalFormat("#.0");
            
            double attendance;
            
            try {
            attendance = Double.parseDouble(df.format(total / count));
            }
            catch(NumberFormatException nfe) {
               attendance = 0.0; 
            }
            year.setStudentAttendanceAverage(attendance);
        }
        
        double currentAttendance = schoolYears.get(0).getStudentAttendanceAverage();
        
        student.setCurrentAttendanceAverage(currentAttendance);
    }
    
    public String getStudentName(int studentID) {
        
        String sqlStr = "SELECT studentname " +
                        "FROM student " +
                        "WHERE studentid=" + studentID;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String studentName = null;
        
        try {
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();
   
            while(rs.next()) {
                studentName = rs.getString("Studentname");
            }
            
            rs.close();
            stmt.close();
        }
        catch(SQLException sqle) {
            
        }
        
        return studentName;
    }
    
    public double calculateStudentGPA(int studentID){
        
        Connection con =  getDatabaseConnection();
        
        String sqlStr = "SELECT GRADE FROM STUDENTSUBJECT WHERE"
                + " STUDENTID = " + studentID;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        

        try
        {
            
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();
            
            double total = 0;
            double gpa = 0;
            int count = 0;
            
            while (rs.next())
            {
                count++;
                total = total + rs.getDouble("Grade");
            }

            rs.close();
            stmt.close();
            
            gpa = total / count;
            return(gpa);
        }
        catch (SQLException sqle)
        {
            System.out.println("Calculate GPA error");
            sqle.printStackTrace();
        }
        finally{
            closeDatabaseConnection(con);
        }
        return 0;
    }
    
    public double getStudentAverageAttendance(int studentID) {
        
        Connection con = getDatabaseConnection();
        String sqlStr = "SELECT attendance " + 
                        "FROM studentsubject " +
                        "WHERE studentid=" + studentID;
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        double total = 0;
        int count = 0;
        
        try {
            
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();
   
            while(rs.next()) {
                total += rs.getDouble("attendance");
                count++;
            }
            
            rs.close();
            stmt.close();
        }
        catch(SQLException sqle) {
            
        }
        finally {
            closeDatabaseConnection(con);
        }
        
        return total / count;
    }
    
    public int getLatestYear() {
        
        String sqlStr = "SELECT schoolYear " +
                        "FROM student_grades_attendance " +
                        "ORDER BY schoolYear DESC";
        
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        int year = 0;
        
        try {
            stmt = con.prepareStatement(sqlStr);
            rs = stmt.executeQuery();
   
            rs.next();
            
            year = rs.getInt("schoolyear");
            
            rs.close();
            stmt.close();
        }
        catch(SQLException sqle) {
            
        }
        
        return year;
    }
    
    public void updateStudentRecord(int subjectId, int studentId, int columnNum, double value) {
       
        String columnName = null;
       
       switch(columnNum) {
           case 0  : columnName = "termonegrade"; break;
           case 1  : columnName = "termtwograde"; break;
           case 2  : columnName = "termthreegrade"; break;
           case 3  : columnName = "termfourgrade"; break;
           case 4  : columnName = "termfivegrade"; break;
           case 5  : columnName = "termsixgrade"; break;
           case 6  : columnName = "termoneattendance"; break;
           case 7  : columnName = "termtwoattendance"; break;
           case 8  : columnName = "termthreeattendance"; break;
           case 9  : columnName = "termfourattendance"; break;
           case 10 : columnName = "termfiveattendance"; break;
           case 11 : columnName = "termsixattendance"; break;
           default : break;
       }
       
       String sqlStr = "UPDATE student_grades_attendance" + 
                       " SET " + columnName + "=" + value +
                       " WHERE subjectId=" + ++subjectId + 
                       " AND studentId=" + studentId + 
                       " AND schoolyear=2015";
       
       PreparedStatement stmt = null;
       
       try {
            stmt = con.prepareStatement(sqlStr);
            stmt.executeUpdate();
            stmt.close();
        }
        catch(SQLException sqle) {
            
        }   
    }
    
    public void addStudent(int studentId, String studentName, int[] subjects) {
        /*int[]Subjects should contain the subject Ids' of the subjects 
        the student is taking.
        */
        //This method works, it inserts the correct values just fine, but it breaks
        //the staff login for some reason.
        boolean check = false;
        String sqlStr = "insert into STUDENT values (" + studentId + ",'" + studentName + "')";
        
            PreparedStatement stmt = null;
            
            try {
                stmt = con.prepareStatement(sqlStr);
                stmt.executeUpdate();
                stmt.close();
                check = true;
            }
            catch(SQLException sqle) {
            
            }
        if(check){    
            for(int i = 0; i < subjects.length; i++){
                sqlStr = null;
                sqlStr = "insert into STUDENT_GRADES_ATTENDANCE values (" + studentId + "," + subjects[i] + ",2015,"
                            + "NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
        
                stmt = null;
                

                try {
                    stmt = con.prepareStatement(sqlStr);
                    stmt.executeUpdate();
                    stmt.close();
                }
                catch(SQLException sqle) {
            
                }
            }
        }

    }
    
    public void addSubject(int subjectId, String subjectName) {
        String sqlStr = "insert into SUBJECT values(" + subjectId + ",'" + subjectName + "')";
        
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.prepareStatement(sqlStr);
            stmt.executeUpdate();
            stmt.close();
        }
        catch(SQLException sqle) {
            
        }
        
    }
    
    public void enrollStudentOnSubject(int studentId, int subjectId) {
        /*Add entry into the STUDENT_GRADES_ATTENDANCE table*/
        String sqlStr = "insert into STUDENT_GRADES_ATTENDANCE values (" + studentId + "," + subjectId + ",2015,"
                            + "NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)";
        
                PreparedStatement stmt = null;
                

                try {
                    stmt = con.prepareStatement(sqlStr);
                    stmt.executeUpdate();
                    stmt.close();
                }
                catch(SQLException sqle) {
            
                }
    }
 
    public Connection getDatabaseConnection() {
        
        String dbaseUrl = ORACLE_DB_URL;
        String username = ORACLE_DB_UNAME;
        String password = ORACLE_DB_PWORD;
        Connection con = null;

        try {
            DriverManager.registerDriver(
                   new oracle.jdbc.driver.OracleDriver());
            con = DriverManager.getConnection(dbaseUrl, username, password);
        }
        catch (Exception e) {
            String msg = "Cannot establish a connection to the database";
        }
        finally {
            return con;
        }
    }
    
    /**
     * Alternate connection method using the java database.
     * @param flag
     * @return 
     */
    public Connection getDatabaseConnection(boolean flag) {
        
        String dbaseUrl = JAVA_DB_URL;
        String username = JAVA_DB_UNAME;
        String password = JAVA_DB_PWORD;
        Connection con = null;

        try {
            DriverManager.registerDriver(
                   new org.apache.derby.jdbc.ClientDriver());
            con = DriverManager.getConnection(dbaseUrl, username, password);
        }
        catch (Exception e) {
            String msg = "Cannot establish a connection to the database";
        }
        finally {
            return con;
        }
    }
    
    public void closeDatabaseConnection(Connection con) {
        
        try {
            con.close();
        }
        catch (Exception e) {
            String msg = "Problem closing the connection to the database";
        }
    }
    
    public void openConnection() {
        con = getDatabaseConnection();
    }
    
    public void closeConnection() {
        closeDatabaseConnection(con);
    }
}