package SchoolSystemData;

/**
 *
 * @author Daniel Brittain
 */
public class Subject {
    
    private int subjectId;
    private String subjectName;
    
    private double[] grades;
    private double[] attendanceRecord;
    
    public Subject(int subjectId, String subjectName, double[] grades, double[] attendance) {
        
        this.subjectId = subjectId;
        this.subjectName = subjectName;
        this.grades = grades;
        this.attendanceRecord = attendance;
        
    }
    
    public void setGrades(double[] grades) {
        this.grades = grades;
    }
    
    public double[] getGrades() {
        return grades;
    }
    
    public void setAttendance(double[] attendanceRecord) {
        this.attendanceRecord = attendanceRecord;
    }
    
    public double[] getAttendanceRecord() {
        return attendanceRecord;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }
    
    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
    
    public String getSubjectName() {
        return subjectName;
    }
}