package SchoolSystemData;

import java.util.ArrayList;

/**
 *
 * @author Daniel Brittain
 */
public class AcademicYear {
    
    private int schoolYear;
    private double studentGradePointAverage;
    private double studentAttendanceAverage;
    private ArrayList<Subject> subjects;
    
    public AcademicYear(int schoolYear) {
        this.schoolYear = schoolYear;
        subjects = new ArrayList<>();
    }
    
    public int getSchoolYear() {
        return schoolYear;
    }
    
    public void addSubject(int subjectId, String subjectName, double[] grades, double[] attendance) {
        subjects.add(new Subject(subjectId, subjectName, grades, attendance));
    }
    
    public ArrayList<Subject> getSubjects() {
        return subjects;
    }
    
    public void setStudentGradePointAverage(double gpa) {
        studentGradePointAverage = gpa;
    }
    
    public double getStudentGradePointAverage() {
        return studentGradePointAverage;
    }
    
    public void setStudentAttendanceAverage(double attendanceAverage) {
        studentAttendanceAverage = attendanceAverage;
    }
    
    public double getStudentAttendanceAverage() {
        return studentAttendanceAverage;
    }
}
