package SchoolSystemData;

import java.util.ArrayList;

/**
 *
 * @author Daniel Brittain
 */
public class Student {
    
    private int id;
    private String name;
    private double currentGradePointAverage;
    private double currentAttendanceAverage;
    private ArrayList<AcademicYear> academicYears;
    
    public Student(int studentId) {
        
        this.id = studentId;
        academicYears = new ArrayList<>();
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public int getId() {
        return id;
    }
    
    public AcademicYear addAcademicYear(int schoolYear) {
        
        AcademicYear academicYear = new AcademicYear(schoolYear);      
        academicYears.add(academicYear);
        
        return academicYear;
    }
    
    public ArrayList<AcademicYear> getAcademicYears() {
        return academicYears;
    }
    
    public void setCurrentGradePointAverage(double gpa) {
        currentGradePointAverage = gpa;
    }
    
    public double getCurrentGradePointAverage() {
        return currentGradePointAverage;
    }
    
    public void setCurrentAttendanceAverage(double averageAttendance) {
        currentAttendanceAverage = averageAttendance;
    }
    
    public double getCurrentAttendanceAverage() {
        return currentAttendanceAverage;
    }
}
