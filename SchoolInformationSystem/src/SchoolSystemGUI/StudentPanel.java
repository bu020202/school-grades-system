/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SchoolSystemGUI;

import SchoolSystemData.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 *
 * @author Daniel Brittain
 */
public class StudentPanel extends JPanel {

    private final Student student;
    
    public StudentPanel(Student student) {
        this.student = student;
        createAndShowStudentGUI();
    }
    
    private void createAndShowStudentGUI() {
          
        ActionListener logoutEvent = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          
                GUI_Controller.logoutUser();
            }
        };
              
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.WHITE);
        GridBagConstraints gbc = new GridBagConstraints();
        
        JLabel studentWelcome = new JLabel("Welcome " + student.getName());
        
        studentWelcome.setFont(new Font("Ariel",Font.PLAIN,30));
        studentWelcome.setForeground(ApplicationWindow.STAFFORDSHIRE_RED);
            gbc.gridx=0;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=3;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,40,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        this.add(studentWelcome,gbc);
        
        JLabel subjectTables = new JLabel(createScrollableLabel());
        JScrollPane scrollPane = new JScrollPane(subjectTables);
        scrollPane.setPreferredSize(new Dimension(250,300));     
            gbc.gridx=0;
            gbc.gridy=1;
            gbc.gridheight=4;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        this.add(scrollPane,gbc);
        
        JLabel lblID = new JLabel("ID:");
        lblID.setFont(new Font("Ariel",Font.PLAIN,18));
        lblID.setForeground(ApplicationWindow.STAFFORDSHIRE_RED);
            gbc.gridx=1;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.EAST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblID,gbc);
        
        JLabel id = new JLabel();
        id.setText(String.valueOf(student.getId()));
        id.setFont(new Font("Ariel",Font.PLAIN,18));
            gbc.gridx=2;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,10,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(id,gbc);
        
        JLabel lblGpa = new JLabel("GPA:");
        lblGpa.setFont(new Font("Ariel",Font.PLAIN,18));
        lblGpa.setForeground(ApplicationWindow.STAFFORDSHIRE_RED);
            gbc.gridx=1;
            gbc.gridy=2;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.EAST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblGpa,gbc);
        
        JLabel gpa = new JLabel();
        gpa.setText(String.valueOf(student.getCurrentGradePointAverage()));
        gpa.setFont(new Font("Ariel",Font.PLAIN,18));
            gbc.gridx=2;
            gbc.gridy=2;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,10,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(gpa,gbc);
        
        JLabel lblAtt = new JLabel("Attendance:");
        lblAtt.setFont(new Font("Ariel",Font.PLAIN,18));
        lblAtt.setForeground(ApplicationWindow.STAFFORDSHIRE_RED);
            gbc.gridx=1;
            gbc.gridy=3;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,10,0,0);
            gbc.anchor = GridBagConstraints.EAST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblAtt,gbc);
        
        JLabel attendance = new JLabel();
        attendance.setText(String.valueOf(student.getCurrentAttendanceAverage()) + "%");
        attendance.setFont(new Font("Ariel",Font.PLAIN,18));
            gbc.gridx=2;
            gbc.gridy=3;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,10,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(attendance,gbc);
        
        JButton print = new JButton("Print");
        print.addActionListener(null);
        print.setEnabled(false);
            gbc.gridx=0;
            gbc.gridy=5;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(print,gbc);
        
        JButton logout = new JButton("Logout");
        logout.addActionListener(logoutEvent);
            gbc.gridx=1;
            gbc.gridy=4;
            gbc.gridheight=1;
            gbc.gridwidth=2;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.SOUTHEAST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(logout,gbc);
    }
    
        public String createScrollableLabel() {
        
        ArrayList<AcademicYear> academicYears = student.getAcademicYears();
        AcademicYear currentYear = academicYears.get(0);
        ArrayList<Subject> subjects = currentYear.getSubjects();
        
        String tables = "<html><body>";
        
        for(Subject subject : subjects) {
            
            double[] grades = subject.getGrades();
            double[] attendance = subject.getAttendanceRecord();
            
            tables += "<font size=\"10\" color=F4143A>" + subject.getSubjectName() + "</font>" +
                      "<table>" +
                        "<tr bgcolor=F4143A>" +
                          "<th><font color=FFFFFF>Half-Term</font></th>" +
                          "<th><font color=FFFFFF>Grade</font></th>" +
                          "<th><font color=FFFFFF>Attendance</font></th>" +
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>1</td>" +
                          "<td>" + grades[0] + "</td>" +                    
                          "<td>" + attendanceCheck(0,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>2</td>" +
                          "<td>" + grades[1] + "</td>" +
                          "<td>" + attendanceCheck(1,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>3</td>" +
                          "<td>" + grades[2] + "</td>" +
                          "<td>" + attendanceCheck(2,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>4</td>" +
                          "<td>" + grades[3] + "</td>" +
                          "<td>" + attendanceCheck(3,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>5</td>" +
                          "<td>" + grades[4] + "</td>" +
                          "<td>" + attendanceCheck(4,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>6</td>" +
                          "<td>" + grades[5] + "</td>" +
                          "<td>" + attendanceCheck(5,attendance) + "</td>"+
                        "</tr>" +
                      "</table>";       
        }
        
        tables += "</body></html>";
        return tables;
    }
    
    private String attendanceCheck(int attendanceIndex, double[] attendanceRecord) {    
        return (attendanceRecord[attendanceIndex] < 85) ? 
                "<font color=F4143A>" + attendanceRecord[attendanceIndex] + "</font>" : 
                "" + attendanceRecord[attendanceIndex];
    }
}
