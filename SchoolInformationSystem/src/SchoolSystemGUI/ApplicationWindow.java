package SchoolSystemGUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Daniel Brittain
 */
public class ApplicationWindow {
    
    public static Color STAFFORDSHIRE_RED = new Color(244,20,58);
    private JPanel mainView;
    private JFrame systemFrame;
    
    public ApplicationWindow() {
        createAndShowGUI();
    }
    
    private void createAndShowGUI() {
        
        systemFrame = new JFrame("Staffordshire Acadamy Grade System");
        systemFrame.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        systemFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        systemFrame.setResizable(false);
        systemFrame.setVisible(true);
        systemFrame.setSize(1024,720);
        
        JPanel banner = new JPanel();
        banner.setBackground(Color.WHITE);
        banner.setLayout(new GridBagLayout());
            gbc.gridx=0;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=1;
            gbc.weighty=0;//0.08;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.BOTH;
        systemFrame.add(banner,gbc);
        
        JLabel bannerText = new JLabel(" STAFFORDSHIRE ACADEMY");
        bannerText.setForeground(Color.WHITE);
        bannerText.setBackground(STAFFORDSHIRE_RED);
        bannerText.setFont(new Font("Ariel",Font.PLAIN,40));
        bannerText.setOpaque(true);
            gbc.gridx=0;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=1;
            gbc.weighty=1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.NORTHWEST;
            gbc.fill = GridBagConstraints.HORIZONTAL;
        banner.add(bannerText,gbc);
        
        JLabel logo = new JLabel();
        ImageIcon iconLogo = new ImageIcon("Image/staffsLogo.png");
        logo.setIcon(iconLogo);
            gbc.gridx=1;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0;
            gbc.weighty=0;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.EAST;
            gbc.fill = GridBagConstraints.NONE;
        banner.add(logo,gbc);
               
        mainView = new JPanel();
        mainView.setLayout(new GridBagLayout());
        mainView.setBackground(Color.white);
            gbc.gridx=0;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=1;
            gbc.weighty=0.897;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.BOTH;
        systemFrame.add(mainView,gbc);
        
        JPanel loginPanel = new LoginPanel(LoginPanel.DEFAULT_LOGIN_PROMPT);
            gbc.gridx=0;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=1;
            gbc.weighty=1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        mainView.add(loginPanel,gbc);
            
        JPanel footer = new JPanel();
        footer.setBackground(STAFFORDSHIRE_RED);
            gbc.gridx=0;
            gbc.gridy=2;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=1;
            gbc.weighty=0.005;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.BOTH;
        systemFrame.add(footer,gbc);
        
        systemFrame.revalidate();
        systemFrame.repaint();
        
        loginPanel.requestFocusInWindow();
        loginPanel.getComponent(3).requestFocusInWindow();

    }
    
    public void switchPanel(JPanel panel) {
        
//            gbc.gridx=0;
//            gbc.gridy=0;
//            gbc.gridheight=1;
//            gbc.gridwidth=1;
//            gbc.ipadx=0;
//            gbc.ipady=0;
//            gbc.weightx=1;
//            gbc.weighty=1;
//            gbc.insets = new Insets(0,0,0,0);
//            gbc.anchor = GridBagConstraints.CENTER;
//            gbc.fill = GridBagConstraints.NONE;
//        mainView.add(panel,gbc);
        
        
        mainView.removeAll();
        mainView.add(panel);
        
        systemFrame.revalidate();
        systemFrame.repaint();
        
        if(panel instanceof LoginPanel) {
            panel.requestFocusInWindow();
            panel.getComponent(3).requestFocusInWindow();
        }
    }
}
