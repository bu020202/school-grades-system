package SchoolSystemGUI;

import SchoolSystemData.Student;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

/**
 *
 * @author Daniel Brittain
 */
public class StaffPanel extends JPanel {
    
    private final String[] staffView = new String[] {"Student","Subject"};
    private JLabel table;
    private JComboBox viewList;
    private JComboBox studentSelect;
    private ArrayList<Student> students;
    private int studentId;
    private JLabel lblName;
    private JLabel lblID;
    private JLabel lblGpa;
    private JLabel lblAttendance;
    private JButton update;
    private JPanel updatePanel;
    
    private JComboBox items;
    private JComboBox subjects;
    private JTextField input;
    JLabel staffWelcome;

    public StaffPanel() {
    
        createStaffGui();
    }
    
    private void createStaffGui() {
        
        students = GUI_Controller.getAllStudents();
        
        ActionListener logoutEvent = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          
                GUI_Controller.logoutUser();
            }
        };
        
        ActionListener switchView = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedItem = ((String)viewList.getSelectedItem());
                
                hideStudentLabels();
                studentSelect.setSelectedIndex(0);
                
                switch(selectedItem) {
                    
                    case "Student" : table.setText(populateStudentTable());break;
                    case "Subject" : table.setText(populateSubjectTable());break;
                    default : table.setText(populateStudentTable());break;
                    
                }               
            }
        };
        
        ActionListener showUpdateDialog = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          

                updatePanel.setVisible(true);    
            }
        };
        
        ActionListener updateRecord = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          

                updatePanel.setVisible(false);    
            }
        };
        
        ActionListener filterEvent = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          
                String selectedItem = ((String)studentSelect.getSelectedItem());
                String id = null;
                
                try {
                    String[] studentIdName = selectedItem.split("-");
                    if(studentIdName.length == 1) {
                        throw new Exception();
                    }
                    id = studentIdName[0];
                    
                    table.setText(GUI_Controller.createScrollableLabelForStaff(Integer.parseInt(id)));
                    createStudentLabels(Integer.parseInt(id));
                }
                catch(Exception exp) {
                    table.setText(populateStudentTable());
                    hideStudentLabels();
                }
                
            }
        };
       
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.WHITE);
        GridBagConstraints gbc = new GridBagConstraints();
        
        staffWelcome = new JLabel("Welcome to the Staff Page.");
        staffWelcome.setFont(new Font("Ariel",Font.PLAIN,30));
        staffWelcome.setForeground(ApplicationWindow.STAFFORDSHIRE_RED);
            gbc.gridx=0;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=6;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,100,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        this.add(staffWelcome,gbc);
        
        JLabel view = new JLabel("View:");
            gbc.gridx=0;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,10,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(view,gbc);
        
        viewList = new JComboBox(staffView);
        viewList.addActionListener(switchView);
            gbc.gridx=1;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(viewList,gbc);
        
        JLabel lblFilter = new JLabel("Select:");
            gbc.gridx=2;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblFilter,gbc);
        
        studentSelect = new JComboBox(populateFilter());
        studentSelect.addActionListener(filterEvent);
            gbc.gridx=3;
            gbc.gridy=1;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(studentSelect,gbc);
       
        table = new JLabel(populateStudentTable());
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(370,200));
            gbc.gridx=0;
            gbc.gridy=2;
            gbc.gridheight=4;
            gbc.gridwidth=4;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(20,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        this.add(scrollPane,gbc);
        
        lblName = new JLabel();
        lblName.setVisible(false);
            gbc.gridx=5;
            gbc.gridy=2;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(20,0,20,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblName,gbc);
        
        lblID = new JLabel();
        lblID.setVisible(false);
            gbc.gridx=5;
            gbc.gridy=3;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(20,0,20,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblID,gbc);
        
        lblGpa = new JLabel();
        lblGpa.setVisible(false);
            gbc.gridx=5;
            gbc.gridy=4;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(20,0,20,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblGpa,gbc);
        
        lblAttendance = new JLabel();
        lblAttendance.setVisible(false);
            gbc.gridx=5;
            gbc.gridy=5;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(20,0,20,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(lblAttendance,gbc);
        
        update = new JButton("Update Records");
        update.addActionListener(showUpdateDialog);
        update.setEnabled(false);
            gbc.gridx=0;
            gbc.gridy=6;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(update,gbc);
        
        updatePanel = createUpdatePanel();
        updatePanel.setVisible(false);
            gbc.gridx=0;
            gbc.gridy=7;
            gbc.gridheight=1;
            gbc.gridwidth=7;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(updatePanel,gbc);
        
        
        JButton logout = new JButton("Logout");
        logout.addActionListener(logoutEvent);
            gbc.gridx=6;
            gbc.gridy=6;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.WEST;
            gbc.fill = GridBagConstraints.NONE;
        this.add(logout,gbc);
        
    }
    
    private String populateStudentTable() {
        
        //students = GUI_Controller.getAllStudents();
        
        String htmlTable = "<html>" +
                           "<body>" +
                           "<table>" +
                           "<tr bgcolor=F4143A>" +
                             "<th><font color=FFFFFF>ID Number</font></th>" +
                             "<th><font color=FFFFFF>Name</font></th>" +
                             "<th><font color=FFFFFF>Current GPA</font></th>" +
                             "<th><font color=FFFFFF>Attendance</font></th>" +
                           "</tr>";
        
        for(Student student : students) {
            
            int id = student.getId();
            String name = student.getName();
            double gpa = student.getCurrentGradePointAverage();
            double attendance = student.getCurrentAttendanceAverage();
            
            htmlTable += "<tr align=center>" +
                           "<td>" + id + "</td>" +
                           "<td align=left>" + name + "</td>" +
                           "<td>" + gpa + "</td>" +
                           "<td>" + attendance + "</td>" +
                         "</tr>";
        }
        
        htmlTable += "</table></body></html>";
        
        return htmlTable;
    }
    
    private String populateSubjectTable() {
        
        String htmlTable = "<html>" +
                           "<body>" +
                           "<table>" +
                           "<tr bgcolor=F4143A>" +
                             "<th><font color=FFFFFF>Subject ID</font></th>" +
                             "<th><font color=FFFFFF>Name</font></th>" +
                             "<th><font color=FFFFFF>GPA</font></th>" +
                             "<th><font color=FFFFFF>Attendance</font></th>" +
                           "</tr>";
        
        for(int i = 0;i < 5;i++) {
            
            htmlTable += "<tr>" +
                           "<td align=center>" + (i+1) + "</td>" +
                           "<td>" + getSubjectName(i+1) + "</td>" +
                           "<td align=center>--" + "</td>" +
                           "<td align=center>--" + "</td>" +
                         "</tr>";
        }
        
        htmlTable += "</table></body></html>";
        
        return htmlTable;
    }
    
    private String getSubjectName(int subjectId) {
        String subjectName = "--";
        
        switch(subjectId) {
            case 1 : subjectName = "Mathematics";break;
            case 2 : subjectName = "English";break;
            case 3 : subjectName = "Chemisty";break;
            case 4 : subjectName = "History";break;
            case 5 : subjectName = "Geography";break;
            default : break;
        }
        
        return subjectName;
    }
    
    private String[] populateFilter() {
        
        ArrayList<String> names = new ArrayList<>();
        
        for(Student student : students) {
            String nameId = student.getId() + "-" + student.getName();
            names.add(nameId);
        }
        
        String[] nameId = new String[names.size() + 1];
        nameId[0] = "All";
        
        for(int i = 1;i <= names.size() ;i++) {
            nameId[i] = names.get(i - 1);
        }
        
        return nameId;
    }
    
    private void createStudentLabels(int studentId) {
        
        Student student = GUI_Controller.getStudent(studentId);
        
        String name = student.getName();
        int id = student.getId();
        
        this.studentId = id;
        
        double gpa = student.getCurrentGradePointAverage();
        double attendance = student.getCurrentAttendanceAverage();
        
        lblName.setText("Name: " + name);
        lblName.setVisible(true);
        
        lblID.setText("ID: " + id);
        lblID.setVisible(true);
        
        lblGpa.setText("GPA: " + gpa);
        lblGpa.setVisible(true);
        
        lblAttendance.setText("Attendance: " + attendance);
        lblAttendance.setVisible(true);
        update.setEnabled(true);
    }
    
    private void hideStudentLabels() {

        lblName.setVisible(false);
        lblID.setVisible(false);
        lblGpa.setVisible(false);
        lblAttendance.setVisible(false);
        update.setEnabled(false);
        updatePanel.setVisible(false);
        
    }
        private JPanel createUpdatePanel() {
        
        ActionListener updateRecord = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          

                //updatePanel.setVisible(false);
                GUI_Controller.updateStudentRecord(subjects.getSelectedIndex(),studentId,items.getSelectedIndex(),Double.parseDouble(input.getText()));
            }
        };
        
        String[] subjectNames = {"Mathematics","English","Chemistry","History","Geography"};
        String[] records = {"Term 1 - Grade","Term 2 - Grade","Term 3 - Grade",
                                      "Term 4 - Grade","Term 5 - Grade","Term 6 - Grade",
                                      "Term 1 - Attendance","Term 2 - Attendance","Term 3 - Attendance",
                                      "Term 4 - Attendance","Term 5 - Attendance","Term 6 - Attendance"};
        
        JPanel up = new JPanel();
        up.setBackground(Color.WHITE);
        up.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        JLabel lblUpdate = new JLabel();
        lblUpdate.setText("Update");
            gbc.gridx=0;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(lblUpdate,gbc);
        
        items = new JComboBox(records);
            gbc.gridx=1;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(items,gbc);
        
        JLabel lblFor = new JLabel();
        lblFor.setText("for the subject");
            gbc.gridx=2;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(lblFor,gbc);
        
        subjects = new JComboBox(subjectNames);
            gbc.gridx=3;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(subjects,gbc);
        
        JLabel lblWith = new JLabel();
        lblWith.setText("with the value");
            gbc.gridx=4;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(lblWith,gbc);
        
        input = new JTextField(5);
            gbc.gridx=5;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(input,gbc);
        
        JButton btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(updateRecord);
            gbc.gridx=6;
            gbc.gridy=0;
            gbc.gridheight=1;
            gbc.gridwidth=1;
            gbc.ipadx=0;
            gbc.ipady=0;
            gbc.weightx=0.1;
            gbc.weighty=0.1;
            gbc.insets = new Insets(0,0,0,0);
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.NONE;
        up.add(btnUpdate,gbc);
        
        return up;
    }
    
}
