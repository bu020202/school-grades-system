package SchoolSystemGUI;

import SchoolSystem.*;
import SchoolSystemData.*;
import java.util.ArrayList;

/**
 *
 * @author Daniel Brittain
 */
public class GUI_Controller {
    
    private static ApplicationWindow window;
    private static SQLHandler sqlHandler;

    public GUI_Controller() {
        window = new ApplicationWindow();
    }
    
    public static void loginUser(String username, char[]password) {
        
        sqlHandler = SystemController.getSQLHandler();
        sqlHandler.openConnection();
        
        int userID = sqlHandler.loginUser(username, password);

        if(userID == 0) {
            //they are staff
            window.switchPanel(new StaffPanel());
        }
        else if(userID > 0) {
            //they are a student          
            window.switchPanel(new StudentPanel(getStudent(userID)));
            
        }
        else if(userID < 0) {
            //login not reconized
            sqlHandler.closeConnection();
            sqlHandler = null;
            
            window.switchPanel(new LoginPanel(LoginPanel.ERROR_LOGIN_PROMPT));
        }
        else{
            //Here be dragons.
        }      
    }
    
    public static Student getStudent(int studentID) {
        
        Student student = sqlHandler.populateStudentData(studentID);
        
        return student;
    }
    
    public static ArrayList<Student> getAllStudents() {
        
        ArrayList<Student> students = sqlHandler.getAllStudents();
        return students;
    }
    
    public static ArrayList<Subject> getAllSubjects() {
        
        
        return null;
    }
    
    public static void logoutUser() {
        
        sqlHandler.closeConnection();
        sqlHandler = null;
        
        window.switchPanel(new LoginPanel(LoginPanel.DEFAULT_LOGIN_PROMPT));
    }
    
    public static String createScrollableLabelForStaff(int id) {
        
        Student student = getStudent(id);
        
        ArrayList<AcademicYear> academicYears = student.getAcademicYears();
        AcademicYear currentYear = academicYears.get(0);
        ArrayList<Subject> subjects = currentYear.getSubjects();
        
        String tables = "<html><body>";
        
        for(Subject subject : subjects) {
            
            double[] grades = subject.getGrades();
            double[] attendance = subject.getAttendanceRecord();
            
            tables += "<font size=\"10\" color=F4143A>" + subject.getSubjectName() + "</font>" +
                      "<table>" +
                        "<tr bgcolor=F4143A>" +
                          "<th><font color=FFFFFF>Half-Term</font></th>" +
                          "<th><font color=FFFFFF>Grade</font></th>" +
                          "<th><font color=FFFFFF>Attendance</font></th>" +
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>1</td>" +
                          "<td>" + grades[0] + "</td>" +
                          "<td>" + attendanceCheck(0,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>2</td>" +
                          "<td>" + grades[1] + "</td>" +
                          "<td>" + attendanceCheck(1,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>3</td>" +
                          "<td>" + grades[2] + "</td>" +
                          "<td>" + attendanceCheck(2,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>4</td>" +
                          "<td>" + grades[3] + "</td>" +
                          "<td>" + attendanceCheck(3,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>5</td>" +
                          "<td>" + grades[4] + "</td>" +
                          "<td>" + attendanceCheck(4,attendance) + "</td>"+
                        "</tr>" +
                        "<tr align=center>" +
                          "<td>6</td>" +
                          "<td>" + grades[5] + "</td>" +
                          "<td>" + attendanceCheck(5,attendance) + "</td>"+
                        "</tr>" +
                      "</table>";       
        }
        
        tables += "</body></html>";
        return tables;
    }
    
    public static String attendanceCheck(int attendanceIndex, double[] attendanceRecord) {    
        return (attendanceRecord[attendanceIndex] < 85) ? 
                "<font color=F4143A>" + attendanceRecord[attendanceIndex] + "</font>" : 
                "" + attendanceRecord[attendanceIndex];
    }
    
    public static void updateStudentRecord(int subjectId, int studentId, int columnNum, double value) {
        sqlHandler.updateStudentRecord(subjectId, studentId, columnNum, value);
    }
}
