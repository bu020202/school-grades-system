package SchoolSystemGUI;

import static SchoolSystemGUI.ApplicationWindow.STAFFORDSHIRE_RED;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Daniel Brittain
 */
public class LoginPanel extends JPanel {
    
    public static String DEFAULT_LOGIN_PROMPT = "Please log in below.";
    public static String ERROR_LOGIN_PROMPT = "Your username or password is incorrect, please try again.";
    
    private JTextField username;
    private JPasswordField password;
    
    public LoginPanel() {
        super();
        
    }
    
    public LoginPanel(String loginpromt) {
        super();
        
        
        ActionListener loginEvent = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {          
                GUI_Controller.loginUser(username.getText(), password.getPassword());
            }
        };
        
        ActionListener clearEvent = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                username.setText(null);
                password.setText(null);
                username.requestFocusInWindow();
            }
        };
        
        this.setBackground(Color.WHITE);
        this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        JLabel welcome = new JLabel("Welcome");
            welcome.setForeground(STAFFORDSHIRE_RED);
            welcome.setFont(new Font("Ariel",Font.PLAIN,64));
                gbc.gridx=0;
                gbc.gridy=0;
                gbc.gridheight=1;
                gbc.gridwidth=2;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.1;
                gbc.weighty=0.1;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(welcome,gbc);
            
            JLabel message = new JLabel(loginpromt);
            if(loginpromt.equals(LoginPanel.ERROR_LOGIN_PROMPT)){
                message.setForeground(STAFFORDSHIRE_RED);
            }
            message.setFont(new Font("Ariel",Font.PLAIN,16));
                gbc.gridx=0;
                gbc.gridy=1;
                gbc.gridheight=1;
                gbc.gridwidth=2;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.1;
                gbc.weighty=0.1;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(message,gbc);
            
            JLabel uname = new JLabel("Username:");
            message.setFont(new Font("Ariel",Font.PLAIN,16));
                gbc.gridx=0;
                gbc.gridy=2;
                gbc.gridheight=1;
                gbc.gridwidth=1;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.1;
                gbc.weighty=0.1;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(uname,gbc);
            
            username = new JTextField(32);
                gbc.gridx=1;
                gbc.gridy=2;
                gbc.gridheight=1;
                gbc.gridwidth=1;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.1;
                gbc.weighty=0.1;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(username,gbc);
            
            JLabel pword = new JLabel("Password:");
            message.setFont(new Font("Ariel",Font.PLAIN,16));
                gbc.gridx=0;
                gbc.gridy=3;
                gbc.gridheight=1;
                gbc.gridwidth=1;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.1;
                gbc.weighty=0.1;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(pword,gbc);
            
            password = new JPasswordField(32);
            password.addActionListener(loginEvent);
                gbc.gridx=1;
                gbc.gridy=3;
                gbc.gridheight=1;
                gbc.gridwidth=1;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.1;
                gbc.weighty=0.1;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(password,gbc);
            
            JPanel buttonSubPanel = new JPanel();
            buttonSubPanel.setLayout(new GridBagLayout());
            buttonSubPanel.setBackground(Color.WHITE);
                gbc.gridx=0;
                gbc.gridy=4;
                gbc.gridheight=1;
                gbc.gridwidth=2;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.5;
                gbc.weighty=0.5;
                gbc.insets = new Insets(0,5,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            this.add(buttonSubPanel,gbc);
            
            JButton loginButton = new JButton("Login");
            loginButton.addActionListener(loginEvent);
                gbc.gridx=0;
                gbc.gridy=0;
                gbc.gridheight=1;
                gbc.gridwidth=1;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.5;
                gbc.weighty=0.5;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            buttonSubPanel.add(loginButton,gbc);
            
            JButton clearButton = new JButton("Clear");
            clearButton.addActionListener(clearEvent);
                gbc.gridx=1;
                gbc.gridy=0;
                gbc.gridheight=1;
                gbc.gridwidth=1;
                gbc.ipadx=0;
                gbc.ipady=0;
                gbc.weightx=0.5;
                gbc.weighty=0.5;
                gbc.insets = new Insets(0,0,0,0);
                gbc.anchor = GridBagConstraints.CENTER;
                gbc.fill = GridBagConstraints.NONE;
            buttonSubPanel.add(clearButton,gbc);
       
            username.requestFocusInWindow();
    }

}
